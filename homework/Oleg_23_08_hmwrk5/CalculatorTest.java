package homework.Oleg_23_08_hmwrk5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @Test
    void sumOfNumbers() {
        assert Calculator.SumOfNumbers(1,2,3,4,5) == 15;
        assert Calculator.SumOfNumbers(10,20,30,40,50) == 150;
    }

    @Test
    void substractingNumbers() {
        assert Calculator.SubstractingNumbers(10,1,2,3,4) == 0;
        assert Calculator.SubstractingNumbers(100,2,2,2,2,2) == 90;
    }

    @Test
    void multiplictionOfNumbers() {
        assert Calculator.MultiplictionOfNumbers(2, 5, 5) == 50;
        assert Calculator.MultiplictionOfNumbers(10, 30, 3) == 900;
    }

    @Test
    void dividingNumbers() {
        assert Calculator.DividingNumbers(100,5,2) == 10;
        assert Calculator.DividingNumbers(100,50,2) == 1;
    }

    @Test
    void findFactorial() {
        assert Calculator.FindFactorial(5) == 120;
        assert Calculator.FindFactorial(4) == 24;
    }
}